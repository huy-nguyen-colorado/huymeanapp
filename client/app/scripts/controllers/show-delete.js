'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ShowDeleteCtrl
 * @description
 * # ShowDeleteCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ShowDeleteCtrl', function ($scope, $routeParams, Show, $location) {
        $scope.show = Show.one($routeParams.id).get().$object;
    
        $scope.deleteShow = function(){
            $scope.show.remove().then(function(){
                $location.path('/shows');
            });
        };
    
        $scope.back = function(){
            $location.path('/show/' + $routeParams.id);  
        };
  });
