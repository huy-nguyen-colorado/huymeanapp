'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ShowsCtrl
 * @description
 * # ShowsCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ShowsCtrl', function ($scope, Show) {
    
        $scope.shows = Show.getList().$object;
    
  });
