'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ShowEditCtrl
 * @description
 * # ShowEditCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ShowEditCtrl', function ($scope, $routeParams, Show, $location) {
        $scope.editShow = true;
    
        $scope.show = {};
    
        Show.one($routeParams.id).get().then(function(show){
            $scope.show = show;
            $scope.saveShow = function(){
                $scope.show.save().then(function(){
                    $location.path('/show/' + $routeParams.id);  
                }); /*end of nested promise */
            };/*end saveShow method*/
        });/*end of restangular promise*/
    
  })/*end controller*/;