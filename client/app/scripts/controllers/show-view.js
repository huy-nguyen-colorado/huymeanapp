'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ShowViewCtrl
 * @description
 * # ShowViewCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ShowViewCtrl', function ($scope, $routeParams, Show) {
        $scope.viewShow = true;
    
        $scope.show = Show.one($routeParams.id).get().$object;
  });
