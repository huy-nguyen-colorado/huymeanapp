'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ShowAddCtrl
 * @description
 * # ShowAddCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ShowAddCtrl', function ($scope, Show, $location) {
        $scope.show = {};
        $scope.saveShow = function(){
            Show.post($scope.show).then(function(){
                $location.path('/shows');
            });  
        };
  });
