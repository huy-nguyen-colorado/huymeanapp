'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
angular
  .module('clientApp', [
    'ngRoute',
    'restangular'
  ])
  .config(function ($routeProvider, RestangularProvider) {
    
    RestangularProvider.setBaseUrl('http://localhost:1776');
    
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/shows', {
        templateUrl: 'views/shows.html',
        controller: 'ShowsCtrl'
      })
      .when('/create/show', {
        templateUrl: 'views/show-add.html',
        controller: 'ShowAddCtrl'
      })
      .when('/show/:id', {
        templateUrl: 'views/show-view.html',
        controller: 'ShowViewCtrl'
      })
      .when('/show/:id/delete', {
        templateUrl: 'views/show-delete.html',
        controller: 'ShowDeleteCtrl'
      })
      .when('/show/:id/edit', {
        templateUrl: 'views/show-edit.html',
        controller: 'ShowEditCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .factory('ShowRestangular', function(Restangular){ 
        return Restangular.withConfig(function(RestangularConfigurer){
            RestangularConfigurer.setRestangularFields({// converts the _id to id, easier this way & more performant
                id: '_id'
            });
        });
  })
 .factory('Show', function(ShowRestangular){
        return ShowRestangular.service('show');
 })
 .directive('huytest', function(){
        return{
            restrict: 'E',
            scope: {
                src: '='
            },
            templateUrl: 'views/huytestdirective.html'
        };
 })
 .filter('trusted', function($sce){
        return function(url){
            return $sce.trustAsResourceUrl(url);
        };
 });