'use strict';

describe('Controller: ShowEditCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var ShowEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShowEditCtrl = $controller('ShowEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
