'use strict';

describe('Controller: ShowDeleteCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var ShowDeleteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShowDeleteCtrl = $controller('ShowDeleteCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
