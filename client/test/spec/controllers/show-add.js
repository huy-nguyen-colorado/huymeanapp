'use strict';

describe('Controller: ShowAddCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var ShowAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShowAddCtrl = $controller('ShowAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
