var mongoose = require('mongoose');

// Create the Show Schema.
var ShowSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true 
    },
    url: {
        type: String,
        required: true
    }
});

// Export the model schema.
module.exports = ShowSchema;